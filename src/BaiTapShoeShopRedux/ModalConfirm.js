import React, { Component } from "react";
import { Button, Modal } from "antd";
import { connect } from "react-redux";
import { CLOSE_MODAL, OK_MODAL } from "./redux/constants/constant";

class ModalConfirm extends Component {
  render() {
    console.log("yes", this.props.isOpenModal);
    return (
      <Modal
        title=""
        visible={this.props.isOpenModal}
        onOk={() => {
          this.props.handleOK();
        }}
        onCancel={() => {
          this.props.handleCancel();
        }}
      >
        Bạn có muốn xoá sản phẩm này???
      </Modal>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isOpenModal: state.shoeShopReducer.isOpenModal,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleOK: () => {
      dispatch({
        type: OK_MODAL,
      });
    },

    handleCancel: () => {
      console.log("yes");
      dispatch({
        type: CLOSE_MODAL,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalConfirm);
