import { dataShoeShop } from "../../dataShoeShope";
import {
  ADD_TO_CARD,
  GIAM_SAN_PHAM,
  TANG_SAN_PHAM,
  CLOSE_MODAL,
  XOA_SAN_PHAM,
  OK_MODAL,
} from "../constants/constant";

let initialState = {
  productList: dataShoeShop,
  gioHang: [],
  isOpenModal: false,
  xoaSP: -1,
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CARD: {
      console.log("yes", payload);
      let cloneGioiHang = [...state.gioHang];
      let index = cloneGioiHang.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index == -1) {
        let newSanPham = { ...payload, soLuong: 1 };
        cloneGioiHang.push(newSanPham);
      } else {
        cloneGioiHang[index].soLuong++;
      }
      state.gioHang = cloneGioiHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case XOA_SAN_PHAM: {
      let cloneGioiHang = [...state.gioHang];

      let index = cloneGioiHang.findIndex((item) => {
        return item.id == payload;
      });

      if (index !== -1) {
        cloneGioiHang.splice(index, 1);

        state.gioHang = cloneGioiHang;

        return { ...state };
      }
    }
    case GIAM_SAN_PHAM: {
      let cloneGioHang = [...state.gioHang];
      console.log({ cloneGioHang });
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      console.log({ index });
      if (index != -1 && cloneGioHang[index].soLuong > 2) {
        cloneGioHang[index].soLuong--;
      } else if (index != -1 && cloneGioHang[index].soLuong == 2) {
        cloneGioHang[index].soLuong--;
      } else {
        state.isOpenModal = true;
        state.xoaSP = index;
      }
      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }
    case TANG_SAN_PHAM: {
      console.log(payload.id);
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      console.log({ index });
      if (index !== -1) {
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }
    case CLOSE_MODAL: {
      state.isOpenModal = false;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }
    case OK_MODAL: {
      state.isOpenModal = false;
      let cloneGioHang = [...state.gioHang];
      cloneGioHang.splice(state.xoaSP, 1);
      state.gioHang = cloneGioHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    default:
      return state;
  }
};
