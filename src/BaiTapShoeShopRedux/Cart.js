import React, { Component } from "react";
import ModalConfirm from "./ModalConfirm";
import { connect } from "react-redux";
import {
  GIAM_SAN_PHAM,
  TANG_SAN_PHAM,
  XOA_SAN_PHAM,
} from "./redux/constants/constant";

class Cart extends Component {
  render() {
    return (
      <div className="container">
        <ModalConfirm isOpenModal={this.props.isOpenModal} />
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phảm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangSanPham(item);
                      }}
                      className="btn btn-success"
                    >
                      Tăng
                    </button>
                    <span className="mx-2">{item?.soLuong}</span>
                    <button
                      onClick={() => {
                        this.props.handleGiamSanPham(item);
                      }}
                      className="btn btn-secondary"
                    >
                      Giảm
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSanPham(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleXoaSanPham: (idSanPham) => {
      dispatch({
        type: XOA_SAN_PHAM,
        payload: idSanPham,
      });
    },
    handleGiamSanPham: (idSanPham) => {
      dispatch({
        type: GIAM_SAN_PHAM,
        payload: idSanPham,
      });
    },
    handleTangSanPham: (idSanPham) => {
      dispatch({
        type: TANG_SAN_PHAM,
        payload: idSanPham,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
