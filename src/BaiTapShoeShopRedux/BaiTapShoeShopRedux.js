import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShope";
import ProductItem from "./ProductItem";
import Productlist from "./Productlist";

class BaiTapShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <Productlist />
        <h4>Số lượng sản phẩm trong giỏ hàng</h4>
        <Cart />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.giaHang,
    isOpenModal: state.shoeShopReducer.isOpenModal,
  };
};

export default connect(mapStateToProps)(BaiTapShoeShopRedux);
