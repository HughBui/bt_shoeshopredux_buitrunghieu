import AnchorFC from "antd/lib/anchor/Anchor";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CARD, XEM_CHI_TIET } from "./redux/constants/constant";

class ProductItem extends Component {
  render() {
    let { name, price, image } = this.props.data;

    return (
      <div className="card col-3  ">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleThemSanPham(this.props.data);
            }}
            className="btn btn-warning"
          >
            Add to card
          </a>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSanPham: (sanPham) => {
      dispatch({
        type: ADD_TO_CARD,
        payload: sanPham,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(ProductItem);
